### Jacopo Barberis 
### Cloud Specialist 2022-2024 
### Fondamenti di version control


# **Git Cheat Sheet**

---

***Indice dei contenuti***

- [Configurazione](#configurazione)
- [Creazione repository](#creazione-repository)
- [Comandi di base](#comandi-di-base)
- [Merge](#merge)
        - [Gestione dei conflitti](#gestione-dei-conflitti)
- [Sincronizzazione con il repository remoto](#sincronizzazione-con-il-repository-remoto)
- [Gestione dei log](#gestione-dei-log)
- [Riscrivere la storia](#riscrivere-la-storia)
- [Modifiche temporanee](#modifiche-temporanee)

---


#### **Configurazione**

##### 1. `git --version`
Se git è installato, mostra la verisone corrente di git.
##### 2. `git config`
Si utilizza per impostare, rimuovere o modificare quello che riguarda le impostazioni git.

*Possibili varianti del comando*:
>- `git config --global user.name "*username*"`, per impostare l'username di git
>- `git config --global user.email "*email*"`, per impostare l'email di git

---

#### **Creazione repository** 

##### 1. `git init`
Si utilizza per inizializzare un repository, il comando di fatto non fa altro che andare a creare una cartella .git all'interno della directory corrente.

##### 2. `git clone`
Crea una copia completa di un repository remoto nel tuo repository locale o tramite HTTPS o tramite chiavi SSH (a differenza del fork che crea una copia ma del repository remoto).

---


#### **Comandi di base**

##### 1. `git add *nome file*`
Aggiunge un file alla sezione di staging, ovvero alla sezione di preparazione al commit.

*Possibili varianti del comando*:
>- `git add -A`, mette in fase di staging tutti i cambiamenti

##### 2. `git reset`
Permette di rimuovere le modifiche presenti nell'area di staging, senza andare a modificare la working directory, ovvero dove siamo lavorando.

*Possibili varianti del comando*:
>- `git reset --hard` , Ha lo stesso funzionamento del comando normale ma applica i cambiamenti anche alla working directory.

##### 3. `git diff *nome file*`
Mostra le differenze tra la working directory, e la sezione di staging.

*Possibili varianti del comando*:
>- `git diff --staged *nome file*` , mostra le differenze tra la sezione di staging e il nostro repository

##### 4. `git status`
Mostra la situazione dei tuoi file. Permette di vedere se sono stati aggiunti, rimossi o modificati dei file, inoltre ti dice anche se sono stati aggiunti o meno alla fase di staging. Tramite un file denominato .gitignore, è possibile far sapere a git quali file non vogliamo che vengano presi in considerazione.

##### 5. `git commit`
Crea un commit contente tutti i cambiamenti che erano presenti in fase di staging e li salva sul repository locale, all'interno del branch nel quale stavamo lavorando. I commit sono quelli che creano la storia di un repository ed è possibile navigarli solo verso il basso, quindi da un commit più recente ad uno meno recente, e mai verso l'alto. Ogni commit ha un id univoco, questo id può poi essere utilizzato per richiamare il commit. 

*Possibili varianti del comando*:
>- `git commit -m` permette di inserire il titolo del commit direttamente mentre si scrive il comando
>- `git commit -a` permette di salvare direttamente tutte le modifiche non inserite in fase di staging ed eseguire il commit con quelle modifiche

##### 6. `git branch`
Mostra tutti i branch che sono stati creati fino a quel momento. Accanto al branch che stiamo utilizzando in quel momento verrà visualizzato un `*`.

*Possibili varianti del comando*:
>- `git branch *nome del branch*` , crea un nuovo branch al commit corrente
>- `git branch -d *nome del branch*` , elimina il branch
>- `git branch -a` , Mostra tutti i branch, locali e remoti

##### 7. `git checkout *id commit*`
Comando che ci permette di navigare la storia del nostro branch riportandoci allo stato delle cose del commit che abbiamo passato al comando.

*Possibili varianti del comando*:
>- `git checkout -b *nome del branch*` , permette di creare un nuovo branch e di spostarsi direttamente all'interno
>- `git checkout *nome branch*` , permette di spostartsi all'interno di un branch

---

#### **Merge**

`git merge`
Permette di unire due sequenze differenti di commit (branch)  creando un commit comune tra loro.

Il comando per il merge va lanciato all'interno del branch di destinazione. 
    Esempio: se dobbiamo fare il merge di un branch feature con il main, il comando per il merge verrà lanciato dal main.

Tipologie di merge:
    - *Fast forward*, quando c'è un percorso lineare dall'head (ultimo commit) del branch corrente all'head del branch di destinazione, git non deve fare un vero e proprio merge, quindi non fa altro che spostare l'head del branch corrente all'head del branch di destinazione.
    - *Three Way Merge*, Sostanzialmente git cerca un commit comune ai due branch, una volta trovato prova ad unire tutte le modifche effettuate dai due branch in un unico commit. Se i due branch dovessero andare ad effettuare modifiche diverse ad uno stesso file, git non riuscirà a completare il merge e genererà un conflitto. Questa tipologia di merge è chiamata così perché git prende i due commit head dei due branch che devono eeguire il merge e il primo commit comune tra loro.

##### Gestione dei conflitti

**Importante**: Fare sempre commit piccoli, questo perché quando ci troveremò poi a risolvere un conflitto, questo sarà molto contenuto e più facilmente risolvibile.

Quando si verifica un conflitto, eseguendo il comando `git status` e possibile vedere quali file sono coinvolti nel conflitto.
Aprendo il file potremo visualizzare le due porzioni di codice interessate e modificarle a nostro piacimento.
Una volta risolto il conflitto si possono eseguire i comandi `git add` e `git commit` per proseguire con il merge.

---

#### Sincronizzazione con il repository remoto

##### 1. `git remote`
Permette di aggiungere o rimuovere i repository remoti con cui puoi interagire dal tuo repository locale,  senza argomenti mostra la lista delle connessioni ai repository remoti.

*Possibili varianti del comando*:
>- `git remote add *nome*` , aggiunge un repository remoto alla lista
>- `git remote rm *nome*` , rimuove un repository remoto dalla lista
>- `git remote -v` , mostra la lista dei repository remoti ma con l'URL

##### 2. `git push`
Permette di inviare tutte le modifiche committate in locale al repositorty remoto, se si ha più di una connessione a repository remoti bisogna specificare nel comando dove vogliamo pushare i nostri cambiamenti.

##### 3. `git fetch`
Permette di scaricare il contenuto di un repository remoto senza però andare ad intaccare il funzionamento del repository locale.

##### 4. `git pull`
Permette di fare il fetch di un repository remoto ma in più esegue anche automaticamente il merge delle modifiche.

---


#### Gestione dei log 

##### 1. `git log`
Mostra la storia dei commit del branch nel quale ci troviamo.

*Possibili varianti del comando*:
>- `git log --oneline` , come il comando normale ma viene messo un commit per riga
>- `git log *nome branch* *nome di un altro branch*` , mostra i commit che sono presenti nel secondo branch ma non nel primo
>- `git log --follow *nome file*` , permette di vedere tutti i commit che hanno modificato quel file, anche se il file ha cambiato nome

##### 2. `git show`
Permette di visualizzare i dettagli di un qualsiasi oggetto di git come, ad esempio, i commit. Se utilizzato per visualizzare un commit mostrerà anche i cambiamenti che sono stati committati all'interno.

---


#### Riscrivere la storia 
##### 1. `git rebase`
Cambia la base di uno o più commit per far si che il punto di creazione del branch sembri diverso. Internamente git crea effettivamente dei commit completamente nuovi, quindi anche se il contenuto è lo stesso l'id dei commit sarà diverso.

##### 2. `git commit --amend`
Metodo veloce per modificare il commit più recente andando ad inserire all'interno dell'ultimo commit le modifiche che sono in staging. Anche in questo caso internamente Git crea un nuovo commit distruggendo il precendente, quindi i commit, quello nuovo e quello vecchio, avranno due id diversi.


#### Modifiche temporanee
##### 1. `git stash`
Permette di salvare "mettendo da parte" delle modifche non ancora pronte al commit, utile per passare velocemente da un contesto ad un altro.

*Possibili varianti del comando*:
>- `git stash pop` , permette di recuperare quello che era stato inserito in stash precendentemente
>- `git stash drop` , permette di eliminare quello che era stato precendentemente inseriro in stash
>- `git stash list` , mostra la lista di tutte le modifiche inserite in stash




